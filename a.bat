@echo off
@setlocal
@set mycmd=java -cp classes Main %*
if "%~1"=="" goto defaultseed
java -jar tester.jar -exec "%mycmd% -seed %*" -seed %*
@GOTO finally
:defaultseed
java -jar tester.jar -exec "%mycmd% -seed 1" -seed 1
@GOTO finally
:finally
@endlocal