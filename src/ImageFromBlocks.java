import java.io.*;
import java.util.*;

public class ImageFromBlocks {

    public static void main(String[] args) throws Exception {
        Main.main(args);
    }

    private int imageHeight, imageWidth, blockSize, fieldHeight, fieldWidth, maxDiscard;
    private int[] fieldStacks, image;
    private int[][] fieldMedians;
    private int dicarded = 0, filled = 0, fieldSize, allSpace = 0;
    private double score = 0.0;
    private int[][][] cellDiffSums;
    private double[] expects;

    public String init(int imageHeight, int blockSize, int maxDiscard, int[] image) {
        this.imageHeight = imageHeight;
        this.imageWidth  = image.length / imageHeight;
        this.blockSize   = blockSize;
        this.fieldHeight = imageHeight / blockSize;
        this.fieldWidth  = imageWidth / blockSize;
        this.maxDiscard  = maxDiscard;
        this.image       = image;

        fieldSize = fieldWidth * fieldHeight;
        fieldStacks = new int[fieldWidth];

        System.err.printf("imageHeight: %d%n", imageHeight);
        System.err.printf("imageWidth: %d%n", imageWidth);
        System.err.printf("blockSize: %d%n", blockSize);
        System.err.printf("fieldHeight: %d%n", fieldHeight);
        System.err.printf("fieldWidth: %d%n", fieldWidth);
        System.err.printf("fieldSize: %d%n", fieldSize);
        System.err.printf("maxDiscard: %d%n", maxDiscard);

        initFieldMedians();

        // Result.initMemo(fieldSize);

        calcExpects();

        return "";
    }

    private void initFieldMedians() {
        cellDiffSums = new int[fieldSize][3][256];
        fieldMedians = new int[fieldSize][3];
        int[] colorR = new int[256];
        int[] colorG = new int[256];
        int[] colorB = new int[256];
        int alls = blockSize * blockSize;
        int mp = alls / 2;
        for (int fy = 0; fy < fieldHeight; fy++) {
            int offY = fy * blockSize;
            for (int fx = 0; fx < fieldWidth; fx++) {
                Arrays.fill(colorR, 0);
                Arrays.fill(colorG, 0);
                Arrays.fill(colorB, 0);
                int offX = fx * blockSize;
                for (int dy = 0; dy < blockSize; dy++) {
                    int e = (offY + dy) * imageWidth + offX;
                    for (int dx = 0; dx < blockSize; dx++) {
                        int c = image[e + dx];
                        colorR[U.getComponent(c, 0)]++;
                        colorG[U.getComponent(c, 1)]++;
                        colorB[U.getComponent(c, 2)]++;
                    }
                }
                int sumR = 0, sumG = 0, sumB = 0;
                for (int i = 0; i < 256; i++) {
                    sumR += i * colorR[i];
                    sumG += i * colorG[i];
                    sumB += i * colorB[i];
                }
                int rc = 0, gc = 0, bc = 0;
                boolean rf = true, gf = true, bf = true;
                int[] meds = fieldMedians[fy * fieldWidth + fx];
                int[][] ds = cellDiffSums[fy * fieldWidth + fx];
                int[] dR = ds[0], dG = ds[1], dB = ds[2];
                dR[0] = sumR;
                dG[0] = sumG;
                dB[0] = sumB;
                for (int i = 0; i < 256; i++) {
                    if (i > 0) {
                        dR[i] = dR[i - 1] + rc - (alls - rc);
                        dG[i] = dG[i - 1] + gc - (alls - gc);
                        dB[i] = dB[i - 1] + bc - (alls - bc);
                    }
                    rc += colorR[i];
                    if (rf && rc > mp) { rf = false; meds[0] = i; }
                    gc += colorG[i];
                    if (gf && gc > mp) { gf = false; meds[1] = i; }
                    bc += colorB[i];
                    if (bf && bc > mp) { bf = false; meds[2] = i; }
                }
            }
        }
    }

    private void calcExpects() {
        expects = new double[fieldSize];
        int[][] averages = new int[3][256];
        long[][][] dp = new long[blockSize + 1][imageWidth + 1][3];
        int p0 = 0, p1 = 1, p2 = blockSize;
        long alls = (long)(blockSize * blockSize);
        for (int y = 0; y < imageHeight; y++) {
            for (int x = 0; x < imageWidth; x++) {
                int c = image[y * imageWidth + x];
                for (int k = 0; k < 3; k++) {
                    dp[p1][x + 1][k] = (long)U.getComponent(c, k)
                        + dp[p1][x][k] + dp[p0][x + 1][k] - dp[p0][x][k];
                }
            }
            p0++;
            if (p0 > blockSize) { p0 = 0; }
            p1++;
            if (p1 > blockSize) { p1 = 0; }
            p2++;
            if (p2 > blockSize) { p2 = 0; }
            if (y + 1 < blockSize) {
                continue;
            }
            for (int x = 0; x < imageWidth; x++) {
                int x1 = x + blockSize;
                if (x1 > imageWidth) { x1 = imageWidth; }
                int c = 0;
                for (int k = 0; k < 3; k++) {
                    long sum = dp[p0][x1][k]
                        - dp[p2][x1][k] - dp[p0][x][k] + dp[p2][x][k];
                    int cc = (int)(sum / alls);
                    if (cc > 0xFF) { cc = 0xFF; }
                    averages[k][cc]++;
                }
            }
        }
        for (int y = imageHeight - blockSize; y < imageHeight; y++) {
            p2++;
            if (p2 > blockSize) { p2 = 0; }
            for (int x = 0; x < imageWidth; x++) {
                int x1 = x + blockSize;
                if (x1 > imageWidth) { x1 = imageWidth; }
                int c = 0;
                for (int k = 0; k < 3; k++) {
                    long sum = dp[p0][x1][k]
                        - dp[p2][x1][k] - dp[p0][x][k] + dp[p2][x][k];
                    int cc = (int)(sum / alls);
                    if (cc > 0xFF) { cc = 0xFF; }
                    averages[k][cc]++;
                }
            }
        }
        double div = (double)image.length * (double)(blockSize * blockSize * 765);
        for (int fy = 0; fy < fieldHeight; fy++) {
            for (int fx = 0; fx < fieldWidth; fx++) {
                int[][] ds = cellDiffSums[fy * fieldWidth + fx];
                long sum = 0;
                for (int k = 0; k < 3; k++) {
                    int[] dd = ds[k];
                    int[] av = averages[k];
                    for (int i = 0; i < 256; i++) {
                        sum += (long)dd[i] * (long)av[i];
                    }
                }
                double err = (double)sum / div;
                expects[fy * fieldWidth + fx] = Math.pow(1.0 - err, 2.0);
                // System.err.printf("fx: %d, fy: %d, expect: %f%n",
                    // fx, fy, expects[fy * fieldWidth + fx]);
            }
        }
    }

    public String placePiece(int pieceHeight, int[] piece, long timeLeft) {
        Piece p = new Piece(pieceHeight, piece);

        Result best = null;
        for (int r = 0; r < 4; r++) {
            p.rotate(r);
            for (int x = 0; x + p.getWidth() <= fieldWidth; x++) {
                Result res = evaluate(p, x);
                if (res == null) {
                    continue;
                }
                if (best == null ||
                    res.space < best.space ||
                    (res.space == best.space &&
                        ((
                        // (res.top < best.top ||
                            // (res.top == best.top &&
                            // res.getRealScore(imageHeight, imageWidth, blockSize, image, p) >
                            res.getScore(fieldHeight, fieldWidth, blockSize, cellDiffSums, p) >
                                // best.getRealScore(imageHeight, imageWidth, blockSize, image, p)
                                best.getScore(fieldHeight, fieldWidth, blockSize, cellDiffSums, p)
                )))) {
                    best = res;
                }
            }
        }

        if (best == null) {
            if (dicarded < maxDiscard) {
                dicarded++;
                return "D";
            } else {
                System.err.println(String.format(
                    "maybe cannot place%nfilled: %d%nspace: %d%nsc: %f%ntime: %d ms%n",
                        filled, allSpace, score, 10000L - timeLeft));
                return p.makeCommand(0);
            }
        }

        // System.err.printf("grs: %f, gs: %f%n",
            // best.calcRealScore(imageHeight, imageWidth, blockSize, image, p),
            // best.calcScore(fieldHeight, fieldWidth, blockSize, cellDiffSums, p)
        // );

        // double expect = 0.0;
        // int n = 0;
        // for (int x = 0; x < fieldWidth; x++) {
            // for (int y = fieldHeight - 1 - fieldStacks[x]; y >= 0 && y >= fieldHeight - 3 - fieldStacks[x]; y--) {
                // n++;
                // expect += expects[y * fieldWidth + x];
            // }
        // }
        // expect /= (double)n;

        // double expect = allExpects() * (double)p.getCount();
        // double expect = pieceExpects(p, best);
        double expect = 0.51 * (double)p.getCount();

        // System.err.printf("ex: %f (%f), bs: %f (%f) %n",
            // expect * (double)p.getCount(), expect,
            // best.getScore(fieldHeight, fieldWidth, blockSize, cellDiffSums, p),
            // best.getScore(fieldHeight, fieldWidth, blockSize, cellDiffSums, p) / (double)p.getCount()
            // );

        if (dicarded < (maxDiscard - fieldWidth / 4) * filled / fieldSize &&
            dicarded < maxDiscard - fieldWidth / 4 &&
            (best.space > 0 ||
                // 1.0 < expect / best.getRealScore(imageHeight, imageWidth, blockSize, image, p)
                1.0 < expect / best.getScore(fieldHeight, fieldWidth, blockSize, cellDiffSums, p)
                )) {
            dicarded++;
            return "D";
        }

        p.rotate(best.rotation);

        place(p, best);

        return p.makeCommand(best.x);
    }

    private double pieceExpects(Piece p, Result r) {
        double ret = 0.0;
        p = p.getRotated(r.rotation);
        int px = r.x;
        int offY = fieldHeight - (r.lower + p.getHeight());
        for (int dy = 0; dy < p.getHeight(); dy++) {
            for (int dx = 0; dx < p.getWidth(); dx++) {
                if (!p.isEmpty(dy, dx)) {
                   ret += expects[(offY + dy) * fieldWidth + px + dx];
                }
            }
        }
        return ret;
    }

    private double allExpects() {
        double ret = 0.0;
        int[] dummy = new int[6];
        Piece p = new Piece(dummy, null);
        int n = 0;
        for (PieceForm pf : PieceForm.forms) {
            p.form = pf;
            for (int x = 0; x + p.getWidth() <= fieldWidth; x++) {
                Result res = evaluate(p, x);
                if (res == null) { continue; }
                if (res.space > 0) { continue; }
                int offY = fieldHeight - (res.lower + p.getHeight());
                double tmp = 0.0;
                for (int dy = 0; dy < p.getHeight(); dy++) {
                    for (int dx = 0; dx < p.getWidth(); dx++) {
                        if (!p.isEmpty(dy, dx)) {
                            tmp += expects[(offY + dy) * fieldWidth + x + dx];
                        }
                    }
                }
                ret += tmp / (double)p.getCount();
                n++;
            }
        }
        return ret / (double)n;
    }

    private Result evaluate(Piece p, int px) {
        int lower = 0, top = 0;
        for (int i = 0; i < p.getWidth(); i++) {
            int y = fieldStacks[i + px] + p.getBottomOffset(i);
            lower = Math.max(lower, y);
        }
        int space = 0;
        for (int i = 0; i < p.getWidth(); i++) {
            top = Math.max(top, lower + p.getTopHeight(i));
            space += lower - p.getBottomOffset(i) - fieldStacks[i + px];
        }
        if (top > fieldHeight) {
            return null;
        } else {
            return new Result(px, lower, space, top, p.getRotation());
        }
    }

    private boolean place(Piece p, Result r) {
        int px = r.x;
        int lower = r.lower;
        allSpace += r.space;
        filled += p.getCount() + r.space;
        // score += r.getRealScore(imageHeight, imageWidth, blockSize, image, p);
        score += r.getScore(fieldHeight, fieldWidth, blockSize, cellDiffSums, p);
        for (int i = 0; i < p.getWidth(); i++) {
            fieldStacks[i + px] = lower + p.getTopHeight(i);
        }
        // printForDebug(lower, p);
        return true;
    }

    private void printForDebug(int lower, Piece p) {
        System.err.println("----------");
        System.err.printf("count: %d%n", p.getCount());
        System.err.printf("lower: %d%n", lower);
        System.err.printf("rotate: %d%n", p.getRotation());
        System.err.printf("sc: %f%n", score);
        for (int i = 0; i < p.getWidth(); i++) {
            String s = String.format("%2d: ", p.getTopHeight(i));
            for (int j = p.getHeight() - 1; j >= 0; j--) {
                if (p.isEmpty(j, i)) {
                    s += " ";
                } else {
                    s += "#";
                }
            }
            System.err.println(s);
        }
        for (int i = 0; i < 4; i++) {
            String s = String.format("%2d: ", fieldStacks[i]);
            for (int j = 0; j < fieldStacks[i]; j++) {
                s += "*";
            }
            System.err.println(s);
        }
    }
}

class U {
    public static int getComponent(int color, int i) {
        return (color >> (i << 3)) & 0xFF;
    }
    public static int getDiff(int color1, int color2) {
        return Math.abs(getComponent(color1, 0) - getComponent(color2, 0))
             + Math.abs(getComponent(color1, 1) - getComponent(color2, 1))
             + Math.abs(getComponent(color1, 2) - getComponent(color2, 2));
    }
    public static int getDiff(int color, int[] colors) {
        return Math.abs(getComponent(color, 0) - colors[0])
             + Math.abs(getComponent(color, 1) - colors[1])
             + Math.abs(getComponent(color, 2) - colors[2]);
    }
}

class Result {
    public static int[][][] memo = null;
    public static void initMemo(int fieldSize) {
        memo = new int[fieldSize][3][256];
        for (int i = 0; i < fieldSize; i++) {
            for (int j = 0; j < 3; j++) {
                Arrays.fill(memo[i][j], -1);
            }
        }
    }
    public final int x, lower, space, top, rotation;
    double score = -1.0;
    public Result(int x, int lower, int space, int top, int rotation) {
        this.x = x;
        this.lower = lower;
        this.space = space;
        this.top = top;
        this.rotation = rotation;
    }
    public double getScore(int fieldHeight, int fieldWidth, int blockSize, int[][][] cellDiffSums, Piece p) {
        if (score < 0.0) {
            score = calcScore(fieldHeight, fieldWidth, blockSize, cellDiffSums, p);
        }
        return score;
    }
    double calcScore(int fieldHeight, int fieldWidth, int blockSize, int[][][] cellDiffSums, Piece p) {
        p = p.getRotated(rotation);
        int offY = fieldHeight - (lower + p.getHeight());
        double sc = 0.0;
        for (int dy = 0; dy < p.getHeight(); dy++) {
            int z = (offY + dy) * fieldWidth + x;
            for (int dx = 0; dx < p.getWidth(); dx++) {
                if (p.isEmpty(dy, dx)) {
                    continue;
                }
                int c = p.getColor(dy, dx);
                int[][] ds = cellDiffSums[z + dx];
                double err = 0.0;
                err += (double)ds[0][U.getComponent(c, 0)];
                err += (double)ds[1][U.getComponent(c, 1)];
                err += (double)ds[2][U.getComponent(c, 2)];
                err /= (double)(blockSize * blockSize * 765);
                sc += Math.pow(1.0 - err, 2.0);
            }
        }
        return sc;
    }
    public double getRealScore(int imageHeight, int imageWidth, int blockSize, int[] image, Piece p) {
        if (score < 0.0) {
            score = calcRealScore(imageHeight, imageWidth, blockSize, image, p);
        }
        return score;
    }
    double calcRealScore(int imageHeight, int imageWidth, int blockSize, int[] image, Piece p) {
        int px = this.x;
        int lower = this.lower;
        int fieldHeight = imageHeight / blockSize;
        int fieldWidth = imageWidth / blockSize;
        p = p.getRotated(rotation);
        double sc = 0.0;
        for (int i = 0; i < p.getWidth(); i++) {
            for (int j = p.getHeight() - 1; j >= 0; j--) {
                if (p.isEmpty(j, i)) {
                    continue;
                }
                int color = p.getColor(j, i);
                int stack = lower + p.getHeight() - j;
                int offX = (i + px) * blockSize;
                int offY = imageHeight - stack * blockSize;
                int z = (fieldHeight - stack) * fieldWidth + i + px;
                double err = 0.0;
                for (int k = 0; k < 3; k++) {
                    int cc = U.getComponent(color, k);
                    if (memo[z][k][cc] < 0) {
                        int diff = 0;
                        for (int dy = 0; dy < blockSize; dy++) {
                            int y = offY + dy;
                            for (int dx = 0; dx < blockSize; dx++) {
                                int x = offX + dx;
                                int c = image[y * imageWidth + x];
                                diff += Math.abs(cc - U.getComponent(c, k));
                            }
                        }
                        memo[z][k][cc] = diff;
                    }
                    err += (double)memo[z][k][cc];
                }
                err /= (double)(blockSize * blockSize * 765);
                sc += Math.pow(1.0 - err, 2.0);
            }
        }
        return sc;
    }
}

class Piece {
    final int[] piece;
    PieceForm form;

    public Piece(int height, int[] piece) {
        this.piece = piece;
        this.form = PieceForm.getForm(height, piece);
    }

    private Piece(Piece p, int r) {
        this.piece = p.piece;
        this.form = p.form.getRotated(r);
    }

    Piece(int[] dummy, PieceForm f) {
        this.piece = dummy;
        this.form = f;
    }

    public int getHeight(){
        return form.height;
    }

    public int getWidth() {
        return form.width;
    }

    public int getColor(int y, int x) {
        return piece[form.getIndex(y, x)];
    }

    public boolean isEmpty(int y, int x) {
        return form.isEmpty(y, x);
    }

    public int getBottomOffset(int x) {
        return form.bottomDiffs[x];
    }

    public int getTopHeight(int x) {
        return form.topHeights[x];
    }

    public void rotate(int r) {
        form = form.getRotated(r);
    }

    public Piece getRotated(int r) {
        if (r == getRotation()) {
            return this;
        } else {
            return new Piece(this, r);
        }
    }

    public void rotateNext() {
        form = form.getNextRotated();
    }

    public int getRotation() {
        return form.rot;
    }

    public int getCount() {
        return form.count;
    }

    public String makeCommand(int x) {
        return Integer.toString(getRotation()) + " " + Integer.toString(x);
    }
}

class PieceForm {
    public final int height, width, rot, pid, count;
    public final int[] indexes, bottomDiffs, topHeights;
    public final boolean[] empties;
    private PieceForm(int h, int w, int r, int p, int[] idx, boolean[] emp) {
        height = h; width = w; rot = r; pid = p << 2; indexes = idx; empties = emp;
        bottomDiffs = new int[w];
        topHeights = new int[w];
        int cnt = 0;
        for (int i = 0; i < w; i++) {
            for (int j = h - 1; j >= 0; j--) {
                if (!emp[j * w + i]) {
                    bottomDiffs[i] = j + 1 - h;
                    break;
                }
            }
            for (int j = 0; j < h; j++) {
                if (!emp[j * w + i]) {
                    topHeights[i] = h - j;
                    break;
                }
            }
            for (int j = 0; j < h; j++) {
                if (!emp[j * w + i]) {
                    cnt++;
                }
            }
        }
        count = cnt;
    }
    public int pos(int y, int x) {
        return y * width + x;
    }
    public int getIndex(int y, int x) {
        return indexes[pos(y, x)];
    }
    public boolean isEmpty(int y, int x) {
        return empties[pos(y, x)];
    }
    public PieceForm getRotated(int r) {
        return forms[pid | r];
    }
    public PieceForm getNextRotated() {
        return getRotated((rot + 1) & 3);
    }
    static final PieceForm[] forms = new PieceForm[40];
    static {
        int[] indexes;
        boolean[] empties;

        // ## [0]  # [1]
        //         #
        indexes = new int[]{0, 1};
        empties = new boolean[]{false , false};
        forms[(0 << 2) | 0] = new PieceForm(1, 2, 0, 0, indexes, empties);
        forms[(0 << 2) | 1] = new PieceForm(2, 1, 1, 0, indexes, empties);

        // ## [2]  # [3]
        //         #
        indexes = new int[]{1, 0};
        forms[(0 << 2) | 2] = new PieceForm(1, 2, 2, 0, indexes, empties);
        forms[(0 << 2) | 3] = new PieceForm(2, 1, 3, 0, indexes, empties);

        // ### [0]  # [1]
        //          #
        //          #
        indexes = new int[]{0, 1, 2};
        empties = new boolean[]{false, false, false};
        forms[(1 << 2) | 0] = new PieceForm(1, 3, 0, 1, indexes, empties);
        forms[(1 << 2) | 1] = new PieceForm(3, 1, 1, 1, indexes, empties);

        // ### [2]  # [3]
        //          #
        //          #
        indexes = new int[]{2, 1, 0};
        forms[(1 << 2) | 2] = new PieceForm(1, 3, 2, 1, indexes, empties);
        forms[(1 << 2) | 3] = new PieceForm(3, 1, 3, 1, indexes, empties);

        // #  [0]  #### [0]  # [1]  ## [0]
        // ##                #      ##
        //                   #
        //                   #
        indexes = new int[]{0, 1, 2, 3};
        empties = new boolean[]{false, false, false, false};
        forms[(2 << 2) | 0] = new PieceForm(2, 2, 0, 2, indexes, new boolean[]{false, true, false, false});
        forms[(3 << 2) | 0] = new PieceForm(1, 4, 0, 3, indexes, empties);
        forms[(3 << 2) | 1] = new PieceForm(4, 1, 1, 3, indexes, empties);
        forms[(4 << 2) | 0] = new PieceForm(2, 2, 0, 4, indexes, empties);

        // ## [2]  #### [2]  # [3]  ## [2]
        //  #                #      ##
        //                   #
        //                   #
        indexes = new int[]{3, 2, 1, 0};
        forms[(2 << 2) | 2] = new PieceForm(2, 2, 2, 2, indexes, new boolean[]{false, false, true, false});
        forms[(3 << 2) | 2] = new PieceForm(1, 4, 2, 3, indexes, empties);
        forms[(3 << 2) | 3] = new PieceForm(4, 1, 3, 3, indexes, empties);
        forms[(4 << 2) | 2] = new PieceForm(2, 2, 2, 4, indexes, empties);

        // ## [1]  ## [1]
        // #       ##
        indexes = new int[]{2, 0, 3, 1};
        forms[(2 << 2) | 1] = new PieceForm(2, 2, 1, 2, indexes, new boolean[]{false, false, false, true});
        forms[(4 << 2) | 1] = new PieceForm(2, 2, 1, 4, indexes, empties);

        //  # [3]  ## [3]
        // ##      ##
        indexes = new int[]{1, 3, 0, 2};
        forms[(2 << 2) | 3] = new PieceForm(2, 2, 3, 2, indexes, new boolean[]{true, false, false, false});
        forms[(4 << 2) | 3] = new PieceForm(2, 2, 3, 4, indexes, empties);

        //  #  [0]  ##  [0]   ## [0]  #   [0]    # [0]
        // ###       ##      ##       ###      ###
        indexes = new int[]{0, 1, 2, 3, 4, 5};
        boolean[] empties1 = new boolean[]{false, false, true,  true, false, false};
        boolean[] empties2 = new boolean[]{true, false, false,  false, false, true};
        forms[(5 << 2) | 0] = new PieceForm(2, 3, 0, 5, indexes, new boolean[]{true, false, true,  false, false, false});
        forms[(6 << 2) | 0] = new PieceForm(2, 3, 0, 6, indexes, empties1);
        forms[(7 << 2) | 0] = new PieceForm(2, 3, 0, 7, indexes, empties2);
        forms[(8 << 2) | 0] = new PieceForm(2, 3, 0, 8, indexes, new boolean[]{false, true, true,  false, false, false});
        forms[(9 << 2) | 0] = new PieceForm(2, 3, 0, 9, indexes, new boolean[]{true, true, false,  false, false, false});

        // ### [2]  ##  [2]   ## [2]  ### [2]  ### [2]
        //  #        ##      ##         #      #
        indexes = new int[]{5, 4, 3, 2, 1, 0};
        forms[(5 << 2) | 2] = new PieceForm(2, 3, 2, 5, indexes, new boolean[]{false, false, false,  true, false, true});
        forms[(6 << 2) | 2] = new PieceForm(2, 3, 2, 6, indexes, empties1);
        forms[(7 << 2) | 2] = new PieceForm(2, 3, 2, 7, indexes, empties2);
        forms[(8 << 2) | 2] = new PieceForm(2, 3, 2, 8, indexes, new boolean[]{false, false, false,  true, true, false});
        forms[(9 << 2) | 2] = new PieceForm(2, 3, 2, 9, indexes, new boolean[]{false, false, false,  false, true, true});

        // #  [1]   # [1]  #  [1]  ## [1]  #  [1]
        // ##      ##      ##      #       #
        // #       #        #      #       ##
        indexes = new int[]{3, 0, 4, 1, 5, 2};
        empties1 = new boolean[]{true, false, false,  false, false, true};
        empties2 = new boolean[]{false, true, false,  false, true, false};
        forms[(5 << 2) | 1] = new PieceForm(3, 2, 1, 5, indexes, new boolean[]{false, true, false,  false, false, true});
        forms[(6 << 2) | 1] = new PieceForm(3, 2, 1, 6, indexes, empties1);
        forms[(7 << 2) | 1] = new PieceForm(3, 2, 1, 7, indexes, empties2);
        forms[(8 << 2) | 1] = new PieceForm(3, 2, 1, 8, indexes, new boolean[]{false, false, false,  true, false, true});
        forms[(9 << 2) | 1] = new PieceForm(3, 2, 1, 9, indexes, new boolean[]{false, true, false,  true, false, false});

        //  # [3]   # [3]  #  [3]   # [3]  ## [3]
        // ##      ##      ##       #       #
        //  #      #        #      ##       #
        indexes = new int[]{2, 5, 1, 4, 0, 3};
        forms[(5 << 2) | 3] = new PieceForm(3, 2, 3, 5, indexes, new boolean[]{true, false, false,  false, true, false});
        forms[(6 << 2) | 3] = new PieceForm(3, 2, 3, 6, indexes, empties1);
        forms[(7 << 2) | 3] = new PieceForm(3, 2, 3, 7, indexes, empties2);
        forms[(8 << 2) | 3] = new PieceForm(3, 2, 3, 8, indexes, new boolean[]{true, false, true,  false, false, false});
        forms[(9 << 2) | 3] = new PieceForm(3, 2, 3, 9, indexes, new boolean[]{false, false, true,  false, true, false});
    }
    public static PieceForm getForm(int height, int[] piece) {
        int width = piece.length / height;
        if (height == 1) {
            if (width == 4) {
                // ####
                return forms[3 << 2];
            } else {
                // ## or ###
                return forms[(width - 2) << 2];
            }
        } else if (width == 2) {
            if (piece[1] < 0) {
                // #
                // ##
                return forms[2 << 2];
            } else {
                // ##
                // ##
                return forms[4 << 2];
            }
        } else if (piece[3] < 0) {
            // ##
            //  ##
            return forms[6 << 2];
        } else if (piece[5] < 0) {
            //  ##
            // ##
            return forms[7 << 2];
        } else if (piece[1] < 0) {
            if (piece[0] < 0) {
                //   #
                // ###
                return forms[9 << 2];
            } else {
                // #
                // ###
                return forms[8 << 2];
            }
        } else {
            //  #
            // ###
            return forms[5 << 2];
        }
    }
}

class Main {

    static int callInit(BufferedReader in, ImageFromBlocks imageFromBlocks) throws Exception {

        int imageHeight = Integer.parseInt(in.readLine());
        int blockSize = Integer.parseInt(in.readLine());
        int maxDiscard = Integer.parseInt(in.readLine());
        int _imageSize = Integer.parseInt(in.readLine());
        int[] image = new int[_imageSize];
        for (int _idx = 0; _idx < _imageSize; _idx++) {
            image[_idx] = Integer.parseInt(in.readLine());
        }

        String _result = imageFromBlocks.init(imageHeight, blockSize, maxDiscard, image);

        System.out.println(_result);
        System.out.flush();

        return 0;
    }

    static int callPlacePiece(BufferedReader in, ImageFromBlocks imageFromBlocks) throws Exception {

        int pieceHeight = Integer.parseInt(in.readLine());
        int _pieceSize = Integer.parseInt(in.readLine());
        int[] piece = new int[_pieceSize];
        for (int _idx = 0; _idx < _pieceSize; _idx++) {
            piece[_idx] = Integer.parseInt(in.readLine());
        }
        long timeLeft = Long.parseLong(in.readLine());

        String _result = imageFromBlocks.placePiece(pieceHeight, piece, timeLeft);

        System.out.println(_result);
        System.out.flush();

        return 0;
    }

    public static void main(String[] args) throws Exception {

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        ImageFromBlocks imageFromBlocks = new ImageFromBlocks();

        callInit(in, imageFromBlocks);

        for (;;) {
            callPlacePiece(in, imageFromBlocks);
        }

    }

}
